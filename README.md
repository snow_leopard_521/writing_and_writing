# 发文和收文

#### 项目介绍
收文管理
实现来文登记、拟办、批办、传阅、承办、转办等功能，通过系统提供Web方式的在线编辑、附件上传功能，可以与Microsoft Word、WPS等文档编辑工具进行嵌入整合。
发文管理
实现发文起草、公文审核、督察督办、发文痕迹保留、公文传阅、公文会审、来文登记等功能。支持多种文件格式，支持对纸质文件扫描录入，可以进行模板红头套用、打印文件和稿纸等功能。


#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)